﻿using System;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
using Lumia.Imaging;
using Lumia.Imaging.Adjustments;

namespace App1
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/test.jpg"));
            var wb = new WriteableBitmap(300, 300);

            using (var imageSource = new StorageFileImageSource(storageFile))
            using (var blurEffect = new BlurEffect(imageSource, 20))
            using (var renderer = new WriteableBitmapRenderer(blurEffect, wb))
            {
                var writeableBitmap = await renderer.RenderAsync();
                Blur.Source = writeableBitmap;
            }
        }
        
    }
}

